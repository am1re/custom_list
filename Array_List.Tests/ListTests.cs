using NUnit.Framework;
using Array_List;
using System;

namespace Array_List.Tests
{
    public class Tests
    {
        [Test]
        public void Add_20_Returned4Count()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3);

            //act
            var first = list[0];

            //assert
            Assert.AreEqual(1, first);
        }

        [Test]
        public void Get_First_1Returned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1,2,3);

            //act
            var first = list[0];

            //assert
            Assert.AreEqual(1, first);
        }

        [Test]
        public void Get_Minus1Index_ReturnedIndexOutOfRange()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3);

            //act
            try
            {
                var first = list[-1];
            }
            catch(Exception ex)
            {
                //assert
                Assert.AreEqual(new IndexOutOfRangeException().Message, ex.Message);
            }
        }

        [Test]
        public void Clear_0Returned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3);

            //act
            list.Clear();
            int count = list.Count;

            //arrange
            Assert.AreEqual(0, count);
        }        
        
        [Test]
        public void Contains_TrueReturned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3);

            //act
            var isContains = list.Contains(2);

            //arrange
            Assert.AreEqual(true, isContains);
        }

        [Test]
        public void Contains_FalseReturned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>();

            //act
            var isContains = list.Contains(4);

            //arrange
            Assert.AreEqual(false, isContains);
        }

        [Test]
        public void  IndexOf_1_1Returned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(0, 1, 2);

            //act
            var position = list.IndexOf(1);

            //arrange
            Assert.AreEqual(1, position);
        }

        [Test]
        public void IndexOf_10_Minus1Returned()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(0, 1, 2);

            //act
            var position = list.IndexOf(10);

            //arrange
            Assert.AreEqual(-1, position);
        }

        [Test]
        public void Remove_1_Returned2Count()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(0, 1, 2);

            //act
            list.Remove(1);
            var newElemAtPosition = list[1]; 

            //arrange
            Assert.AreEqual(2, list.Count);
            Assert.AreEqual(2, newElemAtPosition);          
        }

        [Test]
        public void RemoveAt_2_Returned3Count()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(0, 1, 1, 4);

            //act
            list.RemoveAt(2);
            var newElemAtPosition = list[2];

            //arrange
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual(4, newElemAtPosition);
        }

        [Test]
        public void RemoveAt_2_ReturnedIndexOutOfRangeException()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 0);

            try
            {
                //act
                list.RemoveAt(2);
            }
            catch (Exception ex)
            {
                //arrange
                Assert.AreEqual(new IndexOutOfRangeException().Message, ex.Message);
            }
        }

        [Test]
        public void Insert_AtPosition1Value5_Return3Count()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1,2);

            //act
            list.Insert(1, 5);
            int count = list.Count;
            var newItem = list[1];

            //arrange
            Assert.AreEqual(3, count);
            Assert.AreEqual(5, newItem);
        }

    }
}